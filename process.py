import os
import json

from tqdm import tqdm


def processFile(fname):
    data = {}
    with open(fname,'r') as f:
        for line in f:
            if 'GEEK.geekitemPreload = ' in line:
                data = json.loads(line.split('GEEK.geekitemPreload = ')[1].strip()[:-1])
    return data

filesToParse = []
with open('files.index','r') as f:
    for line in f:
        if os.path.isfile(line.strip()):
            filesToParse.append(line.strip())

print("Found %d files" % len(filesToParse))


with open("boardgamegeek.json","w") as f:
    for fname in tqdm(filesToParse):
        try:
            f.write(json.dumps(processFile(fname)) + "\n")
        except:
            pass


