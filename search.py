"""
Run using 
python3 search.py > games.html && \
    scp games.html zns@cowyo.com:/mnt/volume-nyc1-01/www/schollz.com/games.html
"""
import os
import json
import gzip
import sys

from tabulate import tabulate
from tqdm import tqdm

headers = ['Rank','Game','Minimum players','Play time (minutes)']
easycoop = []
hardcoop = []
easyreg = []
hardreg = []
with tqdm(total=90524) as pbar:
    with gzip.open("boardgamegeek.json.gz","r") as f:
        for line in f:
            pbar.update(1)
            data = json.loads(line.decode('utf-8'))
            if data['item']['subtype'] == 'boardgameexpansion':
                continue
            try:
                score = data['item']['stats']['baverage']
                count = data['item']['stats']['usersrated']
                minplayers = float(data['item']['minplayers'])
                if minplayers == 1:
                    continue
                players = data['item']['minplayers'] + "-" + data['item']['maxplayers']
                gamelength = data['item']['maxplaytime']
                difficulty = float(data['item']['polls']['boardgameweight']['averageweight'])
                name = data['item']['name']
                url = 'https://boardgamegeek.com' + data['item']['href']
                title = "<a href='%s'>%s</a>" % (url,name)
                mechanics = []
                for mechanic in data['item']['links']['boardgamemechanic']:
                    mechanics.append(mechanic['name'])
                gamerow = [title,str(minplayers),str(gamelength),float(score)]
                if float(score) > 5.0 and 'Co-operative Play' in mechanics:
                    if difficulty < 2.4:
                        easycoop.append(gamerow)
                    if difficulty > 2.4:
                        hardcoop.append(gamerow)
                if float(score) > 7.0 and 'Co-operative Play' not in mechanics:
                    if difficulty < 2.4:
                        easyreg.append(gamerow)
                    if difficulty > 2.4:
                        hardreg.append(gamerow)
            except:
                pass

table = []
for i, row in enumerate(sorted(easycoop, key=lambda tup: tup[3], reverse=True)):
    table.append([i+1]+row[:-1])
    if i == 19:
        break
print("\n\n## Easy-to-play co-op games\n\n")
print(tabulate(table, headers, tablefmt="html"))


table = []
for i, row in enumerate(sorted(hardcoop, key=lambda tup: tup[3], reverse=True)):
    table.append([i+1]+row[:-1])
    if i == 19:
        break
print("\n\n## Hard-to-play co-op games\n\n")
print(tabulate(table, headers, tablefmt="html"))

table = []
for i, row in enumerate(sorted(easyreg, key=lambda tup: tup[3], reverse=True)):
    table.append([i+1]+row[:-1])
    if i == 19:
        break
print("\n\n## Easy-to-play games\n\n")
print(tabulate(table, headers, tablefmt="html"))

table = []
for i, row in enumerate(sorted(hardreg, key=lambda tup: tup[3], reverse=True)):
    table.append([i+1]+row[:-1])
    if i == 19:
        break
print("\n\n## Hard-to-play games\n\n")
print(tabulate(table, headers, tablefmt="html"))



